# Jenkins


### Guided Tour

> This guided tour will use the "standalone" Jenkins distribution which requires a 
minimum of Java 7, though Java 8 is recommended. A system with more than 512MB of RAM is also recommended.


1. Download Jenkins.
2. Open up a terminal in the download directory and run java -jar jenkins.war
3. Browse to http://localhost:8080 and follow the instructions to complete the installation.
4. Many Pipeline examples require an installed Docker on the same computer as Jenkins.

When the installation is complete, start putting Jenkins to work and create a [Pipeline](https://jenkins.io/doc/book/pipeline/).

* [Create your first Pipeline](https://jenkins.io/doc/pipeline/tour/hello-world/)

1. Copy one of the [examples below](https://jenkins.io/doc/pipeline/tour/hello-world/#examples) into your repository and name it Jenkinsfile
2. Click the New Item menu within Jenkins.    
![Pipeline](new-item-selection.png)
3. Provide a name for your new item (e.g. My Pipeline) and select Multibranch Pipeline
4. Click the Add Source button, choose the type of repository you want to use and fill in the details.
5. Click the Save button and watch your first Pipeline run!

> You may need to modify one of the example Jenkinsfile's to make it run with your project. Try modifying the sh command to run the same command you would run on your local machine.

> After you have setup your Pipeline, Jenkins will automatically detect any new Branches or Pull Requests that are created in your repository and start running Pipelines for them.


[Continue to "Run multiple steps"](https://jenkins.io/doc/pipeline/tour/running-multiple-steps/)


References
----------

* [Jenkins Oficial](https://jenkins.io/)
* [Download Jenkins 2.46.3](http://mirrors.jenkins.io/war-stable/latest/jenkins.war)
* [Download Jenkins 2.63](http://mirrors.jenkins.io/war/latest/jenkins.war)
* [Jenkins Documentation](https://jenkins.io/doc/)

